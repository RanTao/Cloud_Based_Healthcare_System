<?php
require_once('config.php');
//session_start初始化 $_session保存会话信息
if ( ! session_id() ) @ session_start();
// connect mysql
if(!($con = mysqli_connect(HOST, USERNAME, PASSWORD))){
    echo mysqli_error($con);
}
// select db
if(!(mysqli_select_db($con, 'augustpulse'))){
    echo mysqli_error($con);
}
if(!(mysqli_query($con,'set names utf8'))){
    echo mysqli_error($con);
}
