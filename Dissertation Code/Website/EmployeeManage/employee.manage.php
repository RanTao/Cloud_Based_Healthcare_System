<?php
require_once('connect.php');
include('islogin.php');




$sql = "select * from employees $where order by employee_id asc";
$query  = mysqli_query($con,$sql);

if($query&&mysqli_num_rows($query)){
    while($row =mysqli_fetch_assoc($query)){

        $data[] = $row;

    }

}







?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Employee Manage</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquerys.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>



<table class="table table-bordered" style="margin:10px;">
    <caption style="text-align:center;">
        <h3>
            Employee Manage
        </h3>
    </caption>
    <thead>
    <tr>
        <!--<td>&nbsp;Id</td>-->
        <td>&nbsp;Employee Name</td>
        <td>&nbsp;Address</td>
        <td>&nbsp;Phone Number</td>
        <td>&nbsp;Email</td>
        <td>Operation</td>
    </tr>
    </thead>
    <tbody>
    <?php
    if(!empty($data)){
        foreach($data as $value){
            ?>
            <tr>
                <!--<td>&nbsp;<?php /*echo $value['employee_id']*/?></td>-->
                <td>&nbsp;<?php echo $value['employee_name']?></td>
                <td>&nbsp;<?php echo $value['address']?></td>
                <td>&nbsp;<?php echo $value['phone_number']?></td>
                <td>&nbsp;<?php echo $value['email']?></td>
                <td style="padding:10px;">
                    <a href="employee.del.handle.php?employee_id=<?php echo $value['employee_id']?>" onclick="if (confirm('Are you sure to delete this employee?')) return true; else return false ">
                        Delete
                    </a>
                    <a href="employee.modify.php?employee_id=<?php echo $value['employee_id']?>">
                        Modify
                    </a>
                </td>
            </tr>
            <?php
        }
    }
    ?>
    <tr>
        <td style="height:50px;line-height: 50px;vertical-align: middle;">
            <a href="employee.add.php">
                <input type="button" class="btn btn-default" value="Add Employee">
            </a>
            <a href="userManage.php">
                <input type="button" class="btn btn-default" value="Administer Manage">
            </a>
            <a href="logout.php">
                <input type="button" class="btn btn-default" value="Logout">
            </a>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>
