<?php
require_once('connect.php');
include('islogin.php');


$sql = "select * from login $where order by id asc";
$query  = mysqli_query($con,$sql);

if($query&&mysqli_num_rows($query)){
    while($row =mysqli_fetch_assoc($query)){

        $data[] = $row;

    }

}else{

    $data = array();

}




?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Administer Manage</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquerys.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>



<table class="table table-bordered" style="margin:10px;">
    <caption style="text-align:center;">
        <h3>
            Administers Manage
        </h3>
    </caption>
    <thead>

    <tr>
        <td>&nbsp;Email</td>
        <td>Operation</td>
    </tr>
    </thead>
    <tbody>
    <?php
    if(!empty($data)){
        foreach($data as $value){
            ?>
            <tr>
                <td>&nbsp;<?php echo $value['email']?></td>
                <td style="padding:10px;">
                    <a href="userDel.php?employee_id=<?php echo $value['id']?>" onclick="if (confirm('Are you sure to delete this administer?')) return true; else return false ">
                        Delete
                    </a>
                    <a href="modifyUser.php?id=<?php echo $value['id']?>">
                        Modify
                    </a>
                </td>
            </tr>
            <?php
        }
    }
    ?>
    <tr>
        <td style="height:50px;line-height: 50px;vertical-align: middle;">
            <a href="employee.manage.php">
                <input type="button" class="btn btn-default" value="Manage Employee">
            </a>
            <a href="logout.php">
                <input type="button" class="btn btn-default" value="Logout">
            </a>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>