<?php
require_once('connect.php');
include('islogin.php');
//读取旧信息
$id = $_GET['id'];
$query = mysqli_query($con,"select * from login where id=$id");
$data = mysqli_fetch_assoc($query);
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Add employee</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquerys.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>

<h3 style="text-align:center;">Administer Modify</h3>
<hr/>
<form class="form-horizontal" action="userUpdate.php" method="post" role="form">
    <div class="form-group">
        <label for="firstname" class="col-sm-2 control-label">
           Email:
            <input type="hidden" name="id" value="<?php echo $data['id']?>" />
        </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="email" id="firstname" value="<?php echo $data['email']?>">
        </div>
    </div>
    <div class="form-group">
        <label for="firstname" class="col-sm-2 control-label">
            Oldpassword:
        </label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="oldpassword" id="firstname" >
        </div>
    </div>
    <div class="form-group">
        <label for="firstname" class="col-sm-2 control-label">
            Password:
        </label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="password" id="firstname" >
        </div>
    </div>

    <div class="form-group">
        <label for="firstname" class="col-sm-2 control-label">
            Confirm Password:
        </label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="cfpassword" id="firstname" >
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit</button>
            <a href="userManage.php">
                <button type="button" style="margin-left:50px;" class="btn btn-default">Back</button>
            </a>
        </div>
    </div>
</form>
</body>
</html>