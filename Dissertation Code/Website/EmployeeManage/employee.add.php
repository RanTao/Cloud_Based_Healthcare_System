<?php
include('islogin.php');
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Add Employee</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="bootstrap/js/jquerys.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
</head>
<body>

<h3 style="text-align:center;">Add Employee</h3>
<hr/>
<form class="form-horizontal" action="employee.add.handle.php" method="post" role="form">
    <div class="form-group">
        <label for="firstname" class="col-sm-2 control-label">
            Employee Name:
        </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="employee_name" id="firstname" placeholder="">
        </div>
    </div>

    <div class="form-group">
        <label for="firstname" class="col-sm-2 control-label">
            Address:
        </label>
        <div class="col-sm-10">
            <input type="text" name="address" class="form-control" id="firstname" placeholder="">
        </div>
    </div>

    <div class="form-group">
        <label for="firstname" class="col-sm-2 control-label">
            Email:
        </label>
        <div class="col-sm-10">
            <input type="text" name="email" class="form-control" id="firstname" placeholder="">
        </div>
    </div>

    <div class="form-group">
        <label for="firstname" class="col-sm-2 control-label">
           Phone Number:
        </label>
        <div class="col-sm-10">
            <input type="text" name="phone_number" class="form-control" id="firstname" placeholder="">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit</button>
            <a href="employee.manage.php">
                <button type="button" style="margin-left:50px;" class="btn btn-default">Back</button>
            </a>
        </div>
    </div>
</form>






</body>
</html>