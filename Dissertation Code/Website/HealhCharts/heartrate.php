<?php
require_once('connect.php');
//correspond files
require_once ("jpgraph/src/jpgraph.php");
require_once ("jpgraph/src/jpgraph_line.php");

//read data
$id=isset($_REQUEST['id'])?$_REQUEST['id']:1;
$sql = "select * from heartrate order by heart_id asc ";
$query  = mysqli_query($con,$sql);
$time=array();
$rate=array();
if($query&&mysqli_num_rows($query)) {
    while ($row = mysqli_fetch_assoc($query)) {
        $time[] =  date('m-d H',strtotime($row['date']));
        $rate[] =$row['heart_rate'];
    }
}



if(isset($rate[0]) && !empty($rate[0])){
    $data1 =$rate;//Array of the first curve
    $ydata=$time;//time array
    $graph = new Graph(700,300);
    $graph->SetScale("textlin");
    $graph->SetShadow();
    $graph->img->SetMargin(60,30,30,70); //Set image margins

    $graph->graph_theme = null; //Set the theme to null, otherwise value->Show(); is invalid

    $lineplot1=new LinePlot($data1); //Create two curve objects
    $lineplot1->value->SetColor("red");// set color
    $lineplot1->value->Show();
    $graph->Add($lineplot1);  //Place the curve on the image

    $graph->title->Set(iconv("UTF-8","GB2312//IGNORE","Heart Rate Chart"));   //Set title
    $graph->xaxis->title->Set(iconv("UTF-8","GB2312//IGNORE","time")); //Set the axis name
    $graph->yaxis->title->Set(iconv("UTF-8","GB2312//IGNORE","heart rate"));
    $graph->title->SetMargin(10);
    $graph->xaxis->title->SetMargin(10);
//
    $graph->yaxis->title->SetMargin(10);

    $graph->title->SetFont(FF_FONT1,FS_BOLD); //set font
    $graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
    $graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
    $graph->xaxis->SetTickLabels($ydata);
    $graph->Stroke();  //Output the image
}
else{
    echo "<script>alert('User has not rate data.!'); window.location.herf = //".WEBHOST."employee.manage.php'</script> ";
    header('refresh:0;url=//'.WEBHOST.'/employee.manage.php');
}
