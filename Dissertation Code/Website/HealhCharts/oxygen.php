<?php
require_once('connect.php');
//include('islogin.php');
require_once ("jpgraph/src/jpgraph.php");
require_once ("jpgraph/src/jpgraph_line.php");

$id=isset($_REQUEST['id'])?$_REQUEST['id']:1;
$sql = "select * from oxygen_rate order by oxygen_id asc ";
$query  = mysqli_query($con,$sql);

$time=array();
$oxygen=array();

if($query&&mysqli_num_rows($query)) {
    while ($row = mysqli_fetch_assoc($query)) {
        $time[] =  date('m-d H',strtotime($row['date']));
        $oxygen[] =$row['oxygen_rate'];
    }
}



if(isset($oxygen[0]) && !empty($oxygen[0])){
    $data1 =$oxygen;
    $ydata=$time;

    $graph = new Graph(700,300);
    $graph->SetScale("textlin");
    $graph->SetShadow();
    $graph->img->SetMargin(60,30,30,70); //set margin

    $graph->graph_theme = null;

    $lineplot1=new LinePlot($data1); //set object
    $lineplot1->value->SetColor("red");
    $lineplot1->value->Show();
    $graph->Add($lineplot1);  //add onto graph

    $graph->title->Set(iconv("UTF-8","GB2312//IGNORE","Oxygen chart"));   //set title
    $graph->xaxis->title->Set(iconv("UTF-8","GB2312//IGNORE","time")); //set name
    $graph->yaxis->title->Set(iconv("UTF-8","GB2312//IGNORE","Oxygen_rate"));

    $graph->title->SetMargin(10);
    $graph->xaxis->title->SetMargin(10);
    $graph->yaxis->title->SetMargin(10);

    $graph->title->SetFont(FF_FONT1,FS_BOLD); //set font
    $graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
    $graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
    $graph->xaxis->SetTickLabels($ydata);

    $graph->Stroke();  //output graph
}
else{
    echo "<script>alert('User has not rate data.!'); window.location.herf = //".WEBHOST."employee.manage.php'</script> ";
    header('refresh:0;url=//'.WEBHOST.'/employee.manage.php');
}

