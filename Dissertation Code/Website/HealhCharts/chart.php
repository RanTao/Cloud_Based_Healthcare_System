<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/13
 * Time: 19:08
 */
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Health Condition</title>
    <link rel="stylesheet" href="http://ec2-34-241-30-45.eu-west-1.compute.amazonaws.com/bootstrap/css/bootstrap.min.css">
    <script src="http://ec2-34-241-30-45.eu-west-1.compute.amazonaws.com/bootstrap/js/jquerys.min.js"></script>
    <script src="http://ec2-34-241-30-45.eu-west-1.compute.amazonaws.com/bootstrap/js/bootstrap.js"></script>
</head>
<body>
<! Border form >
<table class="table table-bordered" style="margin:10px;">
    <caption style="text-align:center;">
        <h3>
            Health Conditions Chart
        </h3>
    </caption>
    <thead>
<tr>
        <td style="height:50px;line-height: 50px;vertical-align: middle;">
            <a onclick="getChart('http://ec2-34-241-30-45.eu-west-1.compute.amazonaws.com/heartrate.php');">
                <button class="btn btn-default">Heart Rate</button>
            </a>
            <a onclick="getChart('http://ec2-34-241-30-45.eu-west-1.compute.amazonaws.com/steps.php');" href="#" style="margin-top:10px;">
                <button class="btn btn-default">Steps</button>
            </a>
            <a onclick="getChart('http://ec2-34-241-30-45.eu-west-1.compute.amazonaws.com/oxygen.php');" href="#" style="padding-top:10px;">
                <button class="btn btn-default">Oxygen Rate</button>
            </a>
        </td>
</tr>
    </thead>
</table>
<! popup >
<div style="width:100%;" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:100%;">
        <div class="modal-content" style="width:100%;">
            <div class="modal-header" style="width:100%;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Data statistics table</h4>
            </div>
            <div id="chart" class="modal-body" style="width:100%;text-align:center;">
                <img id="img" src="" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>
<script>
    //jQuery's src to modify the image will re-request the URL of this image and reload the image.
    // popup
    function getChart(url){
        $('#img').attr('src',url);
        $('#myModal').modal('show');
        return false;
    }


</script>

</body>
</html>