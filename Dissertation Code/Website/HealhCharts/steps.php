<?php
require_once ("jpgraph/src/jpgraph.php");
require_once ("jpgraph/src/jpgraph_bar.php");
require_once('connect.php');

$id=isset($_REQUEST['id'])?$_REQUEST['id']:1;
$sql = "select * from walksteps order by steps_id asc ";

$query  = mysqli_query($con,$sql);
$time=array();
$steps=array();
if($query && mysqli_num_rows($query)) {
    while ($row = mysqli_fetch_assoc($query)) {
        $time[] =  date('m-d',strtotime($row['date']));
        $steps[] =$row['steps'];
    }
}

if(isset($steps[0]) && !empty($steps[0])){
    $data  =$steps;
    $ydata =$time;

    $graph = new Graph(700,300);  //create new project
    $graph->SetScale("textlin");  //set scale
    $graph->SetShadow();          //set shadow
    $graph->img->SetMargin(60,30,40,60); //set margin

    $graph->graph_theme = null; //Set the theme to null, otherwise value->Show(); is invalid

    $barplot = new BarPlot($data);  //set BarPlot project
    $barplot->SetFillColor('blue'); //set color
    $barplot->value->Show(); //show number
    $graph->Add($barplot);  //add into graph

    $graph->title->Set('User Steps');
    $graph->xaxis->title->Set('Day'); //set title and title
    $graph->yaxis->title->Set('Steps');

    $graph->title->SetColor("red");
    $graph->title->SetMargin(10);
    $graph->xaxis->title->SetMargin(5);
    $graph->yaxis->title->SetMargin(15);
    $graph->xaxis->SetTickLabels($ydata);


    $graph->title->SetFont(FF_FONT1,FS_BOLD); //set font
    $graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
    $graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
    $graph->xaxis->SetFont(FF_FONT1,FS_BOLD);

    $graph->Stroke();//Output the image
}
else{
    echo "<script>alert('User has not steps data.!'); window.location.herf ='//".WEBHOST."/employee.manage.php';</script> ";
    header('refresh:0;url=//'.WEBHOST.'/employee.manage.php');
}
