import sys
import logging
import rds_config
import pymysql

#rds settings
rds_host  = "lamp-db-instance.cvdsp4amcj0w.eu-west-1.rds.amazonaws.com"
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name


logger = logging.getLogger()
logger.setLevel(logging.INFO)

conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, connect_timeout=5)

def connect():
    ret = True
    try:
        print("succeeded")
    except:
        ret = False
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")

    logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
    
