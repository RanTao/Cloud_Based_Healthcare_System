-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: lamp-db-instance.cvdsp4amcj0w.eu-west-1.rds.amazonaws.com:3306
-- Generation Time: 2018-08-27 12:00:39
-- 服务器版本： 5.6.39-log
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `augustpulse`
--

-- --------------------------------------------------------

--
-- 表的结构 `employees`
--

CREATE TABLE `employees` (
  `employee_id` smallint(10) NOT NULL,
  `employee_name` varchar(20) DEFAULT NULL,
  `address` varchar(50) NOT NULL,
  `phone_number` varchar(11) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `employees`
--

INSERT INTO `employees` (`employee_id`, `employee_name`, `address`, `phone_number`, `email`, `user_id`) VALUES
(54, 'Ran', 'Essex', '7562285623', '645866709@qq.com', 1),
(55, 'ChangShen', 'London', '7419290045', '645866709@qq.com', 1),
(56, 'Ge', 'Southamputon', '5353445345', '645866709@qq.com', 0),
(57, 'RuiBing', 'Scotland', '7419290045', '645866709@qq.com', 0);

-- --------------------------------------------------------

--
-- 表的结构 `heartrate`
--

CREATE TABLE `heartrate` (
  `heart_id` int(11) NOT NULL,
  `heart_rate` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `heartrate`
--

INSERT INTO `heartrate` (`heart_id`, `heart_rate`, `date`, `user_id`) VALUES
(1, 50, '2018-08-13 02:44:27', 1),
(2, 13, '2018-08-14 00:58:52', 1),
(3, 25, '2018-08-13 02:44:33', 1),
(4, 10, '2018-08-13 02:44:38', 1),
(5, 33, '2018-08-13 02:09:07', 1),
(6, 28, '2018-08-13 02:44:46', 1),
(7, 55, '2018-08-13 02:09:15', 1),
(8, 5, '2018-08-13 02:44:58', 1),
(9, 88, '2018-08-13 02:09:24', 1),
(10, 150, '2018-08-13 02:45:28', 1);

-- --------------------------------------------------------

--
-- 表的结构 `login`
--

CREATE TABLE `login` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `lasttime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `login`
--

INSERT INTO `login` (`id`, `email`, `password`, `lasttime`) VALUES
(16, 'rt17538@essex.ac.uk', '202cb962ac59075b964b07152d234b70', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `oxygen_rate`
--

CREATE TABLE `oxygen_rate` (
  `oxygen_id` int(11) NOT NULL,
  `oxygen_rate` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `oxygen_rate`
--

INSERT INTO `oxygen_rate` (`oxygen_id`, `oxygen_rate`, `date`, `user_id`) VALUES
(3, 55, '0000-00-00 00:00:00', 1),
(2, 88, '2018-08-13 02:50:50', 1),
(1, 66, '2018-08-13 02:50:55', 1),
(4, 33, '2018-08-13 05:42:12', 1),
(5, 22, '2018-08-13 05:42:15', 1),
(6, 77, '2018-08-13 05:42:18', 1),
(7, 58, '2018-08-13 05:42:24', 1),
(8, 99, '2018-09-02 00:57:26', 1),
(9, 100, '2018-08-13 05:42:33', 1),
(10, 43, '2018-08-13 05:42:48', 1);

-- --------------------------------------------------------

--
-- 表的结构 `walksteps`
--

CREATE TABLE `walksteps` (
  `steps_id` int(11) NOT NULL,
  `steps` int(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `walksteps`
--

INSERT INTO `walksteps` (`steps_id`, `steps`, `date`, `user_id`) VALUES
(1, 10, '2018-08-13 02:26:40', 1),
(2, 20, '2018-08-13 02:26:41', 1),
(3, 25, '2018-08-13 02:26:42', 1),
(4, 30, '2018-08-13 02:26:43', 1),
(5, 15, '2018-08-13 02:26:44', 1),
(6, 45, '2018-08-13 02:26:45', 1),
(7, 35, '2018-08-13 02:26:45', 1),
(8, 85, '2018-08-13 02:26:46', 1),
(9, 95, '2018-08-13 02:26:47', 1),
(10, 350, '2018-08-13 02:26:48', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employee_id`),
  ADD UNIQUE KEY `ID_UNIQUE` (`employee_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `employees`
--
ALTER TABLE `employees`
  MODIFY `employee_id` smallint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- 使用表AUTO_INCREMENT `login`
--
ALTER TABLE `login`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
